import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadProprietarioRoutingModule } from './cad-proprietario-routing.module';
import { CadProprietarioComponent } from './cad-proprietario.component';


@NgModule({
  declarations: [
    CadProprietarioComponent
  ],
  imports: [
    CommonModule,
    CadProprietarioRoutingModule
  ]
})
export class CadProprietarioModule { }
