import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImobiliariaRoutingModule } from './cad-imobiliaria-routing.module';
import { CadImobiliariaComponent } from './cad-imobiliaria.component';


@NgModule({
  declarations: [
    CadImobiliariaComponent
  ],
  imports: [
    CommonModule,
    CadImobiliariaRoutingModule
  ]
})
export class CadImobiliariaModule { }
