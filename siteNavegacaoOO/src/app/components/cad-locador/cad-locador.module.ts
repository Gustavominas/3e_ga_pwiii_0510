import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadLocadorRoutingModule } from './cad-locador-routing.module';
import { CadLocadorComponent } from './cad-locador.component';


@NgModule({
  declarations: [
    CadLocadorComponent
  ],
  imports: [
    CommonModule,
    CadLocadorRoutingModule
  ]
})
export class CadLocadorModule { }
