import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadLocadorComponent } from './cad-locador.component';

describe('CadLocadorComponent', () => {
  let component: CadLocadorComponent;
  let fixture: ComponentFixture<CadLocadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadLocadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadLocadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
