import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImovelRoutingModule } from './cad-imovel-routing.module';
import { CadImovelComponent } from './cad-imovel.component';


@NgModule({
  declarations: [
    CadImovelComponent
  ],
  imports: [
    CommonModule,
    CadImovelRoutingModule
  ]
})
export class CadImovelModule { }
