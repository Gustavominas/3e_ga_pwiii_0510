export class Imovel{

    private _endereco: string;
    private _bairro: string;
    private _cep: string;
    private _cidade: string;
    private _uf: string;
    private _qtdQuartos: number;
    private _qtdSalas : number;
    private _qtdBanheiros: number;
    private _qtdCozinhas: number;
    private _andares: number;
    private _complemento: string;
    private _valorVenal: number;
    private _valorLocacao: number;
    private _isLocavel: boolean;
    private _isVenal: boolean;
    private _situacao: string;

    constructor(){
        
            this._endereco = "";
            this._bairro = "";
            this._cep = "";
            this._cidade = "";
            this._uf = "";
            this._qtdQuartos = 0;
            this._qtdSalas = 0;
            this._qtdBanheiros = 0;
            this._qtdCozinhas = 0;
            this._andares = 0;
            this._complemento = "";
            this._valorVenal = 0;
            this._valorLocacao = 0;
            this._isLocavel = true;
            this._isVenal = true;
            this._situacao = "";


    }

    public set endereco(endereco: String){
            this._endereco = endereco;
    }
        public get endereco():String{
            return this._endereco;
        }

        public set bairro(bairro: String){
            this._bairro = bairro;
        }
        public get bairro():String{
            return this.bairro;
        }

        public set cep(cep: String){
            this._cep = cep;
        }
        public get cep():String{
            return this._cep;
        }

        public set cidade(cidade: String){
            this._cidade = cidade;
        }
        public get cidade():String{
            return this._cidade;
        }

        public set uf(uf: String){
            this._uf = uf;
        }
        public get uf():String{
            return this._uf;
        }

        public set qtdQuartos(_qtdQuartos: number){
            this._qtdQuartos = qtdQuartos;
        }
        public get qtdQuartos():number{
            return this._qtdQuartos;
        }

        public set qtdSalas(qtdSalas: number){
            this._qtdSalas = qtdSalas;
        }
        public get qtdSalas():number{
            return this._qtdSalas;
        }

        public set qtdBanheiros(qtdBanheiros: number){
            this._qtdBanheiros = qtdBanheiros;
        }
        public get qtdCozinhas():number{
            return this._qtdCozinhas;
        }

        public set andares(andares: number){
            this._andares = andares;
        }
        public get andares():number{
            return this._andares;
        }

        public set complemento(complemento: String){
            this._complemento = complemento;
        }
        public get complemento():String{
            return this._complemento;
        }

        public set valorVenalme(valorVenal: number){
            this._valorVenal = valorVenal;
        }
        public get valorVenal():number{
            return this._valorVenal;
        }

        public set valorLocacao(valorLocacao: number){
            this.valorLocacao = valorLocacao;
        }
        public get valorLocacao():number{
            return this._valorLocacao;
        }

        public set isLocavel(isLocavel: boolean){
            this._isLocavel = isLocavel;
        }
        public get isLocavel():boolean{
            return this._isLocavel;
        }

        public set isVenal(isVenal: boolean){
            this._isVenal = isVenal;
        }
        public get isVenal():boolean{
            return this._isVenal;
        }

        public set situacao(situacao: String){
            this._situacao = situacao;
        }
        public get situacao():String{
            return this._situacao;
        }
}
